package br.com.itau;

import com.sun.org.apache.xerces.internal.impl.xpath.regex.Match;
import com.sun.org.apache.xpath.internal.SourceTree;
import com.sun.org.apache.xpath.internal.functions.FunctionMultiArgs;

import java.awt.*;
import java.util.ArrayList;
import java.util.Random;
import java.util.function.Function;

public class Main {
    public static void main(String[] args) {

        boolean inicio = true;

        while (inicio) {
            Jogo jogo = new Jogo();

            if (!IO.novoJogo()) {
                inicio = false;
                System.out.println("Final de Jogo, até a Próxima");
            }
        }

    }

}
