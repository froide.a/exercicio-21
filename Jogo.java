package br.com.itau;

public class Jogo {

    public Jogo() {

        //duvida
        Monte monte = new Monte();
        monte.setCartas(monte.getCartas());

        boolean continuarJogo = true;
        int posicao = 0;
        int pontos = 0;

        IO.imprimirMensagemInicial();

        while (continuarJogo) {

            IO.imprimirOpcoes();

            String opcao = IO.lerOpcao();

            if (opcao.equalsIgnoreCase("S")) {
                Carta carta = new Carta();
                 //       monte.retirarCarta(monte.getCartas(), posicao).getNumeroCarta());
                 //       monte.retirarCarta(monte.getCartas(), posicao).getNaipe());

                IO.imprimirCarta(carta);

                //pontos = pontos + verificarPontuacaoCarta(carta.getNumeroCarta());

                if (verificarPontuacao(pontos)) {
                    posicao++;
                } else {
                    continuarJogo = false;
                }

                if (!continuarJogo) {
                    IO.imprimirResultado(pontos);
                }

            } else if (opcao.equalsIgnoreCase("N")) {
                IO.imprimirResultado(pontos);
                continuarJogo=false;

            } else {
                System.out.println("Opção inválida! Responda com 'S' ou 'N'.");
                System.out.println("");
            }
        }

    }

    private int verificarPontuacaoCarta(int numero) {
        if (numero == 11 || numero == 12 || numero == 13) {
            return 10;
        } else {
            return numero;

        }
    }

    private boolean verificarPontuacao(int pontos) {
        if (pontos < 21) {
            return true;
        } else {
            return false;
        }
    }


}
