package br.com.itau;

//Classe usada geralmente para constantes
public enum NumeroCarta {
    AS(1),
    DOIS(2),
    TRES(3),
    QUATRO(4),
    CINCO(5),
    SEIS(6),
    SETE(7),
    OITO(8),
    NOVE(9),
    DEZ(10),
    VALETE(10),
    DAMA(10),
    REI(10);

    //declaração de variavel
    public int valorCarta;

    //Metodo NumeroCarta com valor
    NumeroCarta(int valorCarta) {
        //Construtor ?
        this.valorCarta = valorCarta;
    }

    public int getValorCarta() {
        return valorCarta;
    }
}
