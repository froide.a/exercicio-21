package br.com.itau;

import java.util.Scanner;

public class IO {
    public static void imprimirMensagemInicial() {
        System.out.println("Bem Vindo ao Jogo do Jogo 21");
        System.out.println("Você Ganha se a Soma das cartas Atingir 21 pontos");
        }


    public static void imprimirOpcoes() {
        System.out.println("Deseja comprar uma carta?");
        System.out.println("S - Sim");
        System.out.println("N - Não");
    }

    public static String lerOpcao() {
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

    public static void imprimirCarta(Carta carta) {
        System.out.println(carta.getNumeroCarta() + "de " + carta.getNaipe());
    }

    public static void imprimirResultado(int pontos) {
        if (pontos == 21) {
            System.out.println("Parabéns, você fez 21 pontos! \n");
        } else if (pontos > 21) {
            System.out.println("Você perdeu! Total de " + pontos + " pontos. \n");
        } else {
            System.out.println("Você desistiu com " + pontos + " pontos. Seu mole!\n");
        }
    }

    public static boolean novoJogo() {
        System.out.println("Pressione 'C' para Continuar ou 'F' para Finalizar" );
        System.out.println("");
        if (lerOpcao().equalsIgnoreCase("F")){
            return false;
        } else
            return true;
    }

}
