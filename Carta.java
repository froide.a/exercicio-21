package br.com.itau;

public class Carta {

    // Variaveis da carta
    private Naipe naipe;
    private NumeroCarta numeroCarta;

    //Metodo sem parametro
    public Carta(){
    }

    //Metodo Carta com Naipe e Numero
    public Carta(Naipe naipe, NumeroCarta numeroCarta) {
        //Construtores
        this.naipe = naipe;
        this.numeroCarta = numeroCarta;
    }

    //onde usa o get?
    public Naipe getNaipe() {
        return naipe;
    }

    public NumeroCarta getNumeroCarta() {
        return numeroCarta;
    }

    //onde usa o set?
    public void setNaipe(Naipe naipe) {
        this.naipe = naipe;
    }

    public void setNumeroCarta(NumeroCarta numeroCarta) {
        this.numeroCarta = numeroCarta;
    }

    @Override
    public String toString() {
        String modelo =  "Carta{" + "naipe=" + naipe + ", numerocarta=" + numeroCarta + "}";
        return modelo;
    }
}
