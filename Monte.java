package br.com.itau;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Monte {

    //variavel cartas
    private List<Carta> cartas;

    //Metodo Monte
    public Monte() {
        //armazena o conteudo do laço na variavel cartas
        this.cartas = new ArrayList<>();
        //
        for(Naipe naipe : Naipe.values()){
            for(NumeroCarta numeroCarta : NumeroCarta.values()) {
                Carta carta = new Carta(naipe, numeroCarta);
                cartas.add(carta);

            }
        }
    }

    // Embaralha as cartas
    public List<Carta> embaralhaCartas(List<Carta> cartas) {
        Collections.shuffle(cartas);
        return cartas;
    }

    // Retirar uma carta do baralho
    public Carta retirarCarta(List<Carta> cartas, int posicao){
        return cartas.get(posicao);
    }

    public List<Carta> getCartas() {
        return cartas;
    }

    public void setCartas(List<Carta> cartas) {
         this.cartas = cartas;
    }







}


